using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class spawn_target : MonoBehaviour
{
    public GameObject original;
    System.Random rnd = new System.Random();
    int numberOfTargets = 0;
    float zPosition = 0;
    float xPosition = 0;

    float yPosition = 0;

    float coneForZ = 0;

    float xrotation;
    float yrotation;
    float zrotation;
    void Start()
    {



        for (numberOfTargets = rnd.Next(10, 20); numberOfTargets > 0; numberOfTargets--)
        {
            zPosition = rnd.Next(-60, -5);
            xPosition = rnd.Next(2, 20);
            coneForZ = zPosition * .1f;

            xrotation = rnd.Next(0, 180);
            yrotation = rnd.Next(0, 180);
            zrotation = rnd.Next(0, 180);


            yPosition = rnd.Next(-30, 30) / coneForZ;
            GameObject.Instantiate(original, new Vector3(zPosition, xPosition, yPosition), Quaternion.Euler(xrotation, yrotation, zrotation));

        }


    }
}
